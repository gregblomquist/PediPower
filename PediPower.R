# load the packages we want to glue together for easy power
# simulations; these have **many** dependencies
library(pedantics)
library(synbreed)
# load a base R package to speed up the authored function
library(compiler)

# the basic idea is outlined from pedantics in
# http://www.wildanimalmodels.org/tiki-download_wiki_attachment.php?attId=17
# synbreed offers a way to quickly do variance component estimation on
# the simulated data within R via REML

# the workhorse function from pedantics to simulate data, note you can include maternal/paternal effects
#     phensim(pedigree, traits = 1, randomA = NULL, randomE = NULL, 
#             parentalA = NULL, parentalE = NULL, sampled = NULL, 
#                   records = NULL, returnAllEffects = FALSE)

# for now focus on the simplest case of a single Gaussian trait with
# only direct genetic effects and residual variance to estimate and no
# parental effects; data availability is specified in a vector of 1s
# and 0s to be passed to records

# this the the main function the faster compiled version created below is called PediPower() ;
# it should and should always be used instead of this one 
pPediPower<-function(indat, h2s=seq(from=0.05,to=0.95,by=0.10), nsim=5){

    # check OS, will report Windows, Linux, or Darwin (i.e. Mac)
    sys<-as.character(Sys.info()["sysname"])
    oblit<-"NUL"
    if (sys!="Windows") {oblit<-"/dev/null"}

    # insert some tests to make sure arguments are correct
    # 4 column indat
    if (ncol(indat)!=4) {stop("indat must have 4 columns")}
    if (is.numeric(indat[,4])==FALSE) {stop("indat column 4 must be numeric")}
    if (length(indat[,4] %in% c(0,1)) != length(indat[,4])) {stop("indat column 4 must be only 0s and 1s")}    
    # numeric vector of (0-1) for h2s
    if (is.numeric(h2s)==FALSE) {stop("h2s must be numeric")}
    if (min(h2s)<=0) {stop("h2s must be between 0 and 1")}
    if (max(h2s)>=1) {stop("h2s must be between 0 and 1")}
    # single numeric value for nsim
    if (is.numeric(nsim)==FALSE) {stop("nsim must be numeric")}
    if (length(nsim)!=1) {stop("nsim must be a single integer")}
    is.wholenumber <- function(x, tol = .Machine$double.eps^0.5) { abs(x - round(x)) < tol }
    if (is.wholenumber(nsim)==FALSE) {stop("nsim must be a single integer")}
    # check that the pedigree is valid and properly sorted before continuing
    # this will suppress all messages or warnings but informative errors from phensim do get printed 
    sink(oblit)
    to<-try(simtst<-phensim(indat[,-4], randomA = 0.5, randomE = 0.5)$phenotypes, silent=TRUE)
    if (class(to)=="try-error") { sink(); stop("pedigree problem, try fixPedigree() from pedantics") }
    sink()

    
# trim the pedigree to informative individuals to speed up later calculations;
# get some basic stats on it as a side-effect
ps<-pedigreeStats(indat[,-4], cohorts = NULL, dat = indat[,4], retain='informative', graphicalReport = "n", includeA=FALSE,lowMem=TRUE)
pped<-ps$analyzedPedigree
# make sure these are always character for matching
for (i in 1:3){pped[,i]<-as.character(pped[,i])}
    
# store the original indat
oindat<-indat
# convert to character for matching
for (i in 1:3){indat[,i]<-as.character(indat[,i])}
# remerge data indicator but ensure pedigree order is preserved 
tmp<-merge(data.frame(dumtmpn=1:nrow(pped),pped,stringsAsFactors=FALSE),indat[,c(1,4)],by.x=names(pped)[1],by.y=names(indat)[1],all.x=TRUE,all.y=FALSE)
indat<-tmp[order(tmp[,2]), c(1,3,4,5)]



# move on to the power simulation;     
# make empty container for output
powl<-rep(NA,length(h2s)); class(powl)<-"list"

for (i in 1:length(h2s)){

    if (i==1){
        # put the pedigree in synbreed's format and get A matrix only once
        tmpdat<-indat
        theped<-create.pedigree(tmpdat[,1], tmpdat[,2], tmpdat[,3])
        # put data in format synbreed requires
        row.names(tmpdat)<-tmpdat[,1] # desired for matching in this function
        # can only have numeric trait values, but it insists on having more than 1 phenotype column...
        # here just use the binary indicator column and trick it by repeating it
        tgpd<-create.gpData(pheno=tmpdat[tmpdat[,4]==1,c(4,4)],geno=NULL,map=NULL,pedigree=theped)
        # make A matrix for modeling, only has to be done once for entire power simulation set
        message("calculating A matrix...")
        A <- kin(tgpd,ret="add")
    }

# first simulate the data, "traits" can be separate simulations at the given Va and Vr; ignore the warning this gives 
# later drop some of the phenotypes for the estimation step
ntr<-nsim
message(paste("simulating phenotypic data for h2 =",h2s[i]))    
sink(oblit)
    to2<-try( suppressWarnings( asim<-phensim(indat[,-4], traits = ntr, randomA = h2s[i]*diag(ntr), randomE = (1-h2s[i])*diag(ntr), parentalA = NULL, parentalE = NULL, sampled = NULL, records = NULL, returnAllEffects = FALSE)$phenotypes )   , silent=TRUE)
    if (class(to2)=="try-error") { sink(); stop("problem simulating phenotypes on the pedigree") }    
sink()
# suppresses messages and warnings, errors still printed
# "dev/null" is Mac/linux appropriate, replace with "NUL" on Windows
#summary(asim)

# choose a fairly small subset to use in the variance component estimation 
selsim<-subset(asim, asim[,1] %in% subset(indat[,1], indat[,4]==1))

# now get a heritability estimate from this subset of the simulated data
# put data in format synbreed requires
row.names(selsim)<-selsim[,1] # desired for matching in this function
# can only have numeric trait values, but it insists on having more than 1 phenotype column...
gpd<-create.gpData(pheno=selsim[,-1],geno=NULL,map=NULL,pedigree=theped)
#summary(gpd)
# run the model 
## system.time( h2mod<-gpMod(gpData=gpd, model="BLUP", kin=A, trait=1)) # pretty quick
## summary(h2mod)
## as.numeric(h2mod$fit$sigma[1]) # additive genetic variance component
## sqrt(h2mod$fit$sigma.cov[1,1]) # additive genetic variance component SE
    # if running multicore this could be sped up dramatically by using mclapply()
    # implements some very basic error handling with try()
message(paste("running genetic models on simulated data for h2 =",h2s[i]))
sink(oblit)
mods<-lapply(1:ntr, function(z){
    ret<-rep(NA,5) 
    to3<-try( tmp<-gpMod(gpData=gpd, model="BLUP", kin=A, trait=z) , silent=FALSE)
    if (class(to3)!="try-error") { ret <- c(as.numeric(tmp$fit$sigma[1]), as.numeric(tmp$fit$sigma[2]), as.numeric(tmp$fit$sigma.cov[1,1]), as.numeric(tmp$fit$sigma.cov[2,2]), pt(q=as.numeric(tmp$fit$sigma[1]) / sqrt(tmp$fit$sigma.cov[1,1]),df=nrow(selsim),lower.tail=FALSE) ) }
    return(ret)
    }
   )
sink()
# retain only parts to be used later, the whole gpMod object is too large;
# keep the 2 variance components, their SEs, and a rough p-value from the Va/SE(Va) approximate t-statistic 

modDat<-data.frame(do.call("rbind",mods))
modDat<-na.omit(modDat)        # remove any NAs caused by errors in the synbreed runs     
powl[[i]]<-data.frame(modDat)  # force back into data.frame to remove info about omits, nsim - nrow() gives this anyway

}

retlist<-list(inDat=oindat,analyzedDat=indat,pedStats=ps,A=A,h2s=h2s,nsim=nsim,powl=powl)
return(retlist)

}

# make a compiled version to (hopefully) speed up the function some 
PediPower <- cmpfun(pPediPower)
rm(pPediPower)

# usage examples can be found in the associated Rmd file

