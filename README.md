General Pedigree Quantitative Genetic Power Simulations
==================

`PediPower.R` contains the `R` function `PediPower()` for performing analysis of power to detect additive genetic variance of a single Gaussian trait where (potentially) measured individuals are linked by an arbitrary pedigree ([cf. Wilson et al. 2009](http://www.wildanimalmodels.org/tiki-download_wiki_attachment.php?attId=22)).

It works by linking together functions from `pedantics` to simulate the Gaussian trait many times on a given pedigree with a series of predetermined narrow-sense heritabilities and then estimate the heritability from the simulated data with functions from `synbreed` via restricted maximum likelihood (REML). See the `PediPower-usage.Rmd` and compiled/knitted html files for further details. 

# Requirements

1. A working [installation of `R`](http://www.r-project.org/).
2. The `synbreed` and `pedantics` [packages](http://cran.r-project.org/doc/manuals/r-devel/R-admin.html#Installing-packages) and all of their dependecies.
3. Quite a bit of computing time. The many required rounds of simulation and estimation will occupy a computer for several hours to days depending on the function arguments used and hardware. 

# Copyright

[GPL-3](http://cran.r-project.org/web/licenses/GPL-3)

# Citation

If you have used `PediPower()`, it would be nice to see it cited. Please, use my name (G.E. Blomquist) and the URL where you obtained the function in the citation.

# Bugs, Suggestions, etc.

If you find bugs, please [email me](mailto:gregblomquist@gmail.com). Suggestions to improve the code are also welcome. 
